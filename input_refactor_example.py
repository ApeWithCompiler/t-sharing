# Normalize every letter to uppercase
# Only need to check then for Y and N
def normalize_input(input_text):
  return input_text.upper()

def input_is_continiue(input_text):
  return (input_text == "Y")







# Usage example

# Now add_more only contains "Y" or "N", regardless of
# user input
add_more = normalize_input(input())

# Here the function explains readable what the condition
# extually means.
# So your mind don't need to resolve it while reasoning.
if input_is_continiue(add_more):
  # do stuff
  pass

if not input_is_continiue(add_more):
  # To be more "neat", exit with 0.
  # Apps use a "return number" indicating why they exit.
  # 0 means "all ok, as intendet"
  exit(0)